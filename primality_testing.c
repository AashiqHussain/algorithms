//program for primality testing
#include<stdio.h>
#include<stdlib.h>

int main(){

  int n;
  int q, i, a, m, z, y = 1;
  printf("Enter any number\n");
  scanf("%d", &n);

  q = n - 1;
  for(i = 1; i <= n; i++){
    m = q;
    a = rand()% q + 1;
    z = a;
    
    while(m > 0){
      while (m % 2 == 0){
	z = (z * z) % n;
	m =  m / 2;

      }
      m = m - 1;
      y = (y * z) % n;
    }
  }


  if(y != 1){
    printf("%d not prime\n", n);
  }
  else{
    printf("%d is prime\n", n);

  }
  return (n);
}

